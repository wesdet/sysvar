import sysvar.export as export
import sysvar.sysvars as sysvars


def main():
    s = sysvars.Sysvars()
    s.get_sysvars()

    e = export.Export(s.sysvars)
    e.powershell_script("d:\\", "example.ps1")
    return 0


if __name__ == "__main__":
    main()
