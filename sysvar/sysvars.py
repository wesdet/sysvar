"""sysvars.py

This module handles getting and reading system variables.

"""
import pathlib
import subprocess
import sys


class Sysvars(object):
    """System variables object

    This class contains system variables to be imported and exported and methods for handling them.

    Attributes:
        sysvars (dict): The system variables
    """

    def __init__(self):
        self.sysvars = {}
        self._commands = {
            "linux": "printenv",
            "win32": "set"
        }

    def get_sysvars(self, path=None):
        """Get system variables

        This method reads in and formats system variables into a dictionary format.
        If a file of variables is provided it must be one key-value pair per line where the key and value are
        separated by an equal (=) sign.
        If no file is provided, then an operating system call will be used to get them.

        Args:
            path (str, optional): Path to a file containing system variables. One kev-value pair per line
                separated by and equal (=) sign.
        """

        if path is None:
            vars = subprocess.run(
                [self._commands[sys.platform]],
                stdout=subprocess.PIPE,
                stderr=subprocess.PIPE,
                shell=True
            )

            vars = vars.stdout
            try:
                vars = vars.decode('ASCII')
            except AttributeError:
                pass

        else:
            path = pathlib.Path(path)
            with path.open('r') as f:
                vars = f.read()

        vars = [x.strip() for x in vars.split("\n") if x != '']
        for var in vars:
            k, v = var.split("=", maxsplit=1)
            self.sysvars[k] = v


def main():
    """Module main function"""

    s = Sysvars()
    s.get_sysvars()
    return 0


if __name__ == "__main__":
    """Entry point"""
    main()
