"""export.py

This module exports system variables and formats them into a useful format for setting them
on another system.

"""
import pathlib


class Export(object):
    """Handles functionality for exporting system variables.

    This class contains methods that handle the formatting and writing
    of system variables in a requested format.

    Args:
        sysvars (dict): Key value pairs of system variables.

    Attributes:
        TODO: add attributes or delete section

    """

    def __init__(self, sysvars):
        self._sysvars = sysvars

    def powershell_script(self, output, name="sysvars.ps1"):
        """Write PowerShell script

        This method outputs a *.ps1 PowerShell script.
        The system variables will be set with the command SETX /M

        Args:
            output (str): Path to output script. Can be dir or file. Default is sysvars.ps1
            name (str): Name of the script. The extension .ps1 will be appended if it is not in the provided name.

        """
        path = pathlib.Path(output)
        setx_template = 'SETX /M {key} \"{var}\"\n'

        path = pathlib.Path(path, name)

        if path.suffix != ".ps1":
            path = pathlib.Path(str(path) + ".ps1")

        path.parent.mkdir(parents=True, exist_ok=True)

        header = """<#
Notes:
1) Run the PowerShell script as an administrator.
2) Set the restrictions appropriately.
    Ex. PS C:\: Set-ExecutionPolicy RemoteSigned
    Then select [A] Yes to All

    Execution options:
    Restricted   — Stops any script from running.
    RemoteSigned — Allows scripts created on the device, but scripts created on another computer won't run unless they include a trusted publisher's signature.
    AllSigned    — All the scripts will run, but only if a trusted publisher has signed them.
    Unrestricted — Runs any script without any restrictions.
#>

"""
        path.write_text(header)
        with path.open("a") as f:
            for k, v in self._sysvars.items():
                f.write(setx_template.format(key=k, var=v))


def main():
    """Module main function"""

    # Test system variables
    sysvars = {
        "BOB": "Lob Law",
        "FOO": "C:\\bar",
        "ANSWER": 42
    }

    export = Export(sysvars)
    export.powershell_script("d:\\sysvar_test")
    return 0


if __name__ == "__main__":
    """Entry point"""
    main()
